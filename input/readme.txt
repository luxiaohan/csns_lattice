This file is a description of the file rfdata*.multi / rfdat.dual /*.paint
XXX.multi will be used to simulate the multi harmonic RFNode situation while the XXX.dual files only used to simulate the single or dual harmonic RFNode cases.
time(s) [SynchPhase or Brho] RFVoltage(GV) RFPhase(deg) RFVoltage(2) RFPhase(2) ......
rfdata.multi  is the case RFPhase synchronize with Brho[or synchEnergy], and the second column is Brho
rfdata1.multi is the case RFPhase is zero (treat as phase errors of the rf cavity), and the second column is Brho
rfdata2.multi is the case RFPhase synchronize with Brho[or synchEnergy], and the last second is SynchPhase
rfdata3.multi is the case RFPhase is zero (treat as phase errors of the rf cavity), and the second column is SynchPhase
rfdataFor300MeV.dual is the case that using dual_harmonic_cav Node. From left to right, each columm means "time(s) RFVoltage(GV) RFPhase(deg) RatioVoltage RFPhase2 Brho". Synctron Phase can be calcuated by program automatically, please keep the RFPhase being 0. You can also keep the RatioVoltage 0 to simulate the single harmonic RFNode cases.    

empir.multi contains the real RFVoltage settings of the machine for the moment
(2020-06-03), and the RFPhase is zero

For painting curve.
Time(s), X(m), Y(m)
anti-design-pc1.paint  is an anti-correlated-painting curve. From 0 to 0.39 ms, bumpx from 30mm to 0mm, bumpy from -26mm to 0mm.
anti-design-pc2.paint  is an anti-correlated-painting curve. From 0 to 0.39 ms, bumpx from 30mm to 0mm, bumpy from -26mm to 0mm. Compared with anti-design-pc1, in the vertical plane, its painting is slower at the edges, but faster at the center.
cor-design-pc1.paint  is a correlated-painting curve. The injection process is from 0 to 0.39 ms, bumpx from 30mm to 0mm, bumpy from 0mm to -20mm.